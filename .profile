### gpg-agent for ssh-keys ###
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
    export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
# Set GPG TTY
export GPG_TTY=$(tty)
# Refresh gpg-agent tty in case user switches into an X session
gpg-connect-agent updatestartuptty /bye >/dev/null

# SublimeText git integration
[[ $(which ksshaskpass) ]] && {
    export GIT_ASKPASS=/usr/bin/ksshaskpass
    export SSH_ASKPASS=/usr/bin/ksshaskpass
}
