# no duplicates in path
typeset -U path
path=(~/bin $path)

export LANSRV=192.168.2.80
export MS3_DIFFICULTY="easy"
export EDITOR=/usr/bin/vim
export VISUAL=/usr/bin/vim
