#
# /etc/bash.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ $DISPLAY ]] && shopt -s checkwinsize

#PS1='[\u@\h \W]\$ '

#case ${TERM} in
#  xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
#    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
#
#    ;;
#  screen*)
#    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033_%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
#    ;;
#esac

### Bash Prompt Colors ###
GREEN="\[\033[01;32m\]"
YELLOW="\[\033[01;33m\]"
RED="\[\033[01;31m\]"
BLUE="\[\033[01;34m\]"
CYAN="\[\033[01;36m\]"
DCYAN="\[\033[36m\]"
PURP="\[\033[01;35m\]"
LGREY="\[\033[37m\]"
DGREY="\[\033[90m\]"
RESET="\[\033[00m\]"

### Bash Prompt Colors - 256 ###
#EXAMPLE="\[\033[38;5;${ColorNumber}m\]"
DCYAN="\[\033[38;5;23m\]"
DCYAN2="\[\033[38;5;24m\]"
DBLUE="\[\033[38;5;25m\]"
DGREEN="\[\033[38;5;29m\]"
DGREEN2="\[\033[38;5;28m\]"
BRGREEN="\[\033[38;5;83m\]"
BRGREEN2="\[\033[38;5;84m\]"
BRGREEN3="\[\033[38;5;85m\]"
DRED="\[\033[38;5;124m\]"

# current \u, \h and \w
BRGREEN4="\[\033[38;5;49m\]"
DCYAN3="\[\033[38;5;30m\]"
MGREY="\[\033[38;5;245m\]"
BLUEGREEN="\[\033[38;5;37m\]"

### Background Colors ###

PROMPT_DIRTRIM=2

PS1="${MGREY}[${RED}\u${MGREY}@${BRGREEN4}\h${MGREY}:${BLUEGREEN}\w${MGREY}]\$ ${RESET}"

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion
[[ -f ~/.bash_aliases ]] && . ~/.bash_aliases
