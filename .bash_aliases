#       _ _
#  __ _| (_) __ _ ___  ___  ___
# / _` | | |/ _` / __|/ _ \/ __|
#| (_| | | | (_| \__ \  __/\__ \
# \__,_|_|_|\__,_|___/\___||___/
#

# http://unix.stackexchange.com/questions/1045/ddg#1098
alias tmux='TERM=xterm-256color tmux'

# stupid ghostscript...
alias gs='git status --short'

# Trying out neovim
alias vim=nvim

# I'm learning emacs now, let the religious war begin
alias emacs='emacs -nw'

alias ls='ls -hN --color=auto --group-directories-first'
alias ll='ls -l'
alias la='ls -A'
alias l='ls -ld'
alias lla='ls -Al'
#
## Typing Errors ##
alias cd..='cd ..'
alias lslbk='lsblk'
alias ipa='ip a'

#
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# Old style 'specific' aliases (aka bad habits...)
alias in='sudo pacman -S'
alias iny='sudo pacman -S --noconfirm'
alias se='pacman -Ss'   # search pkg repos
alias sei='pacman -Si'  # search pkg repos, detailed info
alias seii='pacman -Sii'  # search pkg repos, slightly more details than -Si
alias sel='pacman -Qs'  # search local pkg database
alias seli='pacman -Qi' # search local pkg database, detailed info
alias upd='sudo pacman -Syu'    # update arch
alias updy='sudo pacman -Syu --noconfirm'
alias docs='cd ~/Documents' # im lazy
alias dl='cd ~/Downloads'   # more laziness  =]

# New aliases, abbreviations for base commands instead of specific commands
alias p='pacman'
alias sp='sudo pacman'
alias tri='trizen'
alias sc='systemctl'
alias ssc='sudo systemctl'
alias ugrep='systemctl list-unit-files |grep'
alias jc='journalctl'
alias sjc='sudo journalctl'
alias mkd='mkdir -pv'
alias v='vim'
alias sv='sudo vim'
alias ka='killall'
alias grep='grep --color=auto' # Color grep - highlight desired sequence
# Doesn't want to work on my system for some reason...?
#alias which='which -i'  # make which read aliases as well
alias ccat='highlight --out-format=ansi' # Color cat - print file with syntax highlighting
alias ok='okular'
alias gwen='gwenview'
alias sptest='speedtest-cli'
alias diff='diff --color=always'

# git graph
alias git-graph='git log --all --graph'

# yay aliases
alias yget='yay -G'
alias getpkg='yay -G'
alias yse='yay -Ss'
alias ysei='yay -Si'
alias yseii='yay -Sii'
alias ysel='yay -Qs'
alias yseli='yay -Qi'

# trizen aliases
alias tget='trizen -G'
alias tse='trizen -Ss'
alias tsei='trizen -Si'
alias tseii='trizen -Sii'
alias tsel='trizen -Qs'
alias tseli='trizen -Qi'

# reload term config file
if [[ $SHELL == '/bin/bash' ]]; then
    alias rel='. ~/.bashrc'
elif [[ $SHELL == '/bin/zsh' || $SHELL == '/usr/bin/zsh' ]]; then
    alias rel='source ~/.zshrc'
else
    echo 'no $SHELL env var found'
fi

alias shred='shred -f -n 10 -z -u'
alias encrypt-gpg='gpg -r cory -e'
alias gpg-encrypt='gpg -r cory -e'
alias en-gpg='gpg -r cory -e'
alias gpg-en='gpg -r cory -e'
alias scriptrp='scriptreplay'
#alias tree='tree -p'
alias neo='neofetch'
alias yt='youtube-dl --add-metadata -ic' # Download video link
alias yta='youtube-dl --add-metadata -xic' # Download only audio
alias mnt-cd='sudo mount /dev/sr0 /mnt/sr0'
alias umnt-cd='sudo umount /dev/sr0'
#
alias pip='python3 -m pip'
alias less='less -i'
alias ptop='bpytop'
