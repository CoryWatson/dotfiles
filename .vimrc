"        _                         __  _       _ _         _
" __   _(_)_ __ ___  _ __ ___     / / (_)_ __ (_) |___   _(_)_ __ ___
" \ \ / / | '_ ` _ \| '__/ __|   / /  | | '_ \| | __\ \ / / | '_ ` _ \
"  \ V /| | | | | | | | | (__   / /   | | | | | | |_ \ V /| | | | | | |
" (_)_/ |_|_| |_| |_|_|  \___| /_/    |_|_| |_|_|\__(_)_/ |_|_| |_| |_|

" `:config` to open vimrc
command! Config execute ":e $MYVIMRC"

" `:reload` to source vimrc
command! Rel execute ":source $MYVIMRC"
" vim-plug

" Some basics...

    set nocompatible                " vim, not vi
    syntax enable
    set background=dark
    colorscheme elflord
    set ruler                       " show the line number on the bar
    "set more                        " use more prompt
    set autoread                    " watch for file changes

" New leader mappings
    let mapleader="\<Space>"

    """ Buffers
    " cycle between buffers
    nnoremap <silent><Leader>m :bnext<CR>
    nnoremap <silent><Leader>n :bprevious<CR>
    " mappings from tim pope's unimpared.vim plugin
    nnoremap <silent> [b :bprevious<CR>
    nnoremap <silent> ]b :bnext<CR>
    nnoremap <silent> [B :bfirst<CR>
    nnoremap <silent> ]B :blast<CR>
    " close buffer
    nnoremap <Leader>d :bd<CR>
    " close view port (pane)
    nnoremap <silent> <Leader>c <C-w>c
    " only
    nnoremap <silent> <Leader>o <C-w>o

    """ Tabs
    " cycle between tabs
    nnoremap <PageDown> :tabn<CR>
    nnoremap <PageUp>   :tabp<CR>
    " new tab
    nnoremap <Leader>t :tabnew<CR>
    " close tab
    "nnoremap <Leader>c :tabc<CR>

    " remove search highlighting
    nnoremap <silent> <Leader><space> :nohl<CR>

    " sort function (visual mode)
    vnoremap <Leader>s :sort<CR>

    " read an empty HTML template and move cursor to title
    nnoremap <leader>html :-1read $HOME/.vim/.skeleton.html<CR>3jwf<

    " source ~/.vimrc
    nnoremap <Leader>r :source ~/.vimrc<CR>

    " insert `kwargs['']`
    nnoremap <Leader>k akwargs['']<Esc>F'i

    " remap escape
    imap jj <Esc>

    " flake8
    "If any of `g:flake8_show_in_gutter` or `g:flake8_show_in_file` are set to `1`, call:
    let g:flake8_show_in_gutter = 1
    "let g:flake8_show_in_file = 1
    autocmd FileType python map <buffer> <Leader>l :call flake8#Flake8()<CR>
    nnoremap <Leader>e :call flake8#Flake8UnplaceMarkers()<CR>

" Misc. configurations

    set ttyfast                   " we have a fast terminal
    set showmode
    set showcmd
    set smartindent               " auto/smart indent
    "set smarttab                  " tab and backspace are smart
    set expandtab
    set shiftwidth=4
    set tabstop=4
    set expandtab
    set softtabstop=4
    set scrolloff=5               " keep at least 5 lines above/below
    set sidescrolloff=5           " keep at least 5 lines left/right
    set hidden
    set noautowrite               " don't automagically write on :next
    set lazyredraw                " don't redraw when don't have to
    set history=200
    set backspace=indent,eol,start
    set linebreak
    set cmdheight=1               " command line one line high
    set undolevels=1000           " 1000 undos
    set updatecount=100           " switch every 100 chars
    set complete=.,w,b,u,t,i,d    " do lots of scanning on tab completion
    set noerrorbells              " No error bells please
    set shell=zsh
    set fileformats=unix
    set ff=unix
    "set mouse=a                    "enable mouse, ***breaks normal copy and paste***
    " why did I add this line? something about alacritty + tmux???
    "set ttymouse=sgr
    function! ToggleMouse()
        if &mouse == 'a'
            set mouse=
        else
            set mouse=a
        endif
    endfunc
    nnoremap <silent> <c-m> :call ToggleMouse()<CR>
    function! ToggleColorColumn()
        if &colorcolumn == '80'
            set colorcolumn=
        else
            set colorcolumn=80
        endif
    endfunc
    nnoremap <F8> :call ToggleColorColumn()<CR>
    filetype on                   " Enable filetype detection
    filetype indent on            " Enable filetype-specific indenting
    filetype plugin on            " Enable filetype-specific plugins
    set laststatus=2
    set modeline
    set nospell                 " no spell checking
    " Fix auto-indention when pasting
    "set paste
    function! TogglePaste()
        set paste!
    endfunc
    nnoremap <F3> :call TogglePaste()<CR>


" Line Numbering Stuff

    "set number                      " regular line numbers
    set number "relativenumber       " hybrid fixed/relative line numbers

    " Dynamic line numbers
    " Automatically switches between normal and relative line numbers
    " insert mode = regular line numbers
    " all other modes = hybrid-relative numbers
    "augroup numbertoggle
    "  autocmd!
    "  autocmd BufEnter,FocusGained,InsertLeave * set number relativenumber
    "  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
    "augroup END

    " Function to toggle line numbers
    " :set nonumber no longer works after messing with relativenumber
    function! ToggleNumber()
        set number!
        "set relativenumber!
    endfunc

    " ctrl+n to toggle line numbers
    " doesn't break dynamic relative/nonrelative functionality
    nnoremap <c-n> :call ToggleNumber()<CR>

    " Toggle relative line numbers
    function! ToggleRelaNum()
        set relativenumber!
    endfunc
    nnoremap <F4> :call ToggleRelaNum()<CR>


" Autocompletion/Searching Stuff

    " the following tells Vim to look in the directory containing the current file (.),
    " then the current directory (empty text between two commas),
    " then each directory under the current directory ('**')
    set path=.,,**
    "set path+=./**
    set wildmenu                  " menu has tab completion
    set wildmode=longest,list,full
    set incsearch                 " incremental search
    set ignorecase smartcase      " search ignoring case unless caps are used
    set hlsearch                  " highlight the search
    "set showmatch                 " show matching bracket
    let g:loaded_matchparen=1     " DONT show matching bracket
    set diffopt=filler,iwhite     " ignore all whitespace and sync


" Backups

    "set backup
    "set backupdir=~/.vim-backup

    " Don't create backup files when editing in these locations.
    set backupskip=/tmp/*,~/tmp/*


" Split Window Stuff

    set splitbelow splitright   " New split window added to the right/below
    set winminheight=0          " Allow window split borders to touch.
    set winminwidth=0

    " Use ctrl-[hjkl] to select the active split!
    nmap <silent> <c-k> :wincmd k<CR>
    nmap <silent> <c-j> :wincmd j<CR>
    nmap <silent> <c-h> :wincmd h<CR>
    nmap <silent> <c-l> :wincmd l<CR>

    " Adjust split window size (not Mac friendly)
    nnoremap <silent> <c-Left> :vertical resize +3<CR>
    nnoremap <silent> <c-Right> :vertical resize -3<CR>
    nnoremap <silent> <c-Up> :resize +3<CR>
    nnoremap <silent> <c-Down> :resize -3<CR>
    " the following is needed to get Ctrl+arrow-keys to work (at least in alacritty)
    map <ESC>[1;5D <c-Left>
    map <ESC>[1;5C <c-Right>
    map <ESC>[1;5B <c-Down>
    map <ESC>[1;5A <c-Up>

    " Adjust split window size (old)
    "nnoremap <silent> <Leader>+ <C-w>10>
    "nnoremap <silent> <Leader>_ <C-w>10<
    "nnoremap <silent> <Leader>= <C-w>5+
    "nnoremap <silent> <Leader>- <C-w>5-
    "nnoremap <C-,> <C-w>+
    "nnoremap <C-S-PageDown> <C-w>-
    "nnoremap + <c-w>+
    "nnoremap - <c-w>-

    " Removes the pipe symbols that seperate split windows
    set fillchars+=vert:\


" Mappings and other useful stuff

    " Insert newline (no insert-mode)
    nmap oo m`o<Esc>``
    nmap OO m`O<Esc>``

    " Fix long timeout for 'o' and 'O'
    set timeoutlen=175  " be careful of this over slow connections

"    " Seems to throw off vim when editing git commit messages?
"    " Make sure Vim returns to the same line when you reopen a file.
"    " Thanks, Amit
"    augroup line_return
"        au!
"        au BufReadPost *
"            \ if line("'\"") > 0 && line("'\"") <= line("$") |
"            \     execute 'normal! g`"zvzz' |
"            \ endif
"    augroup END

    " 'Y' yanks from cursor to eol instead of yanking entire line like 'yy'
    nnoremap Y y$

    " remap j and k to scroll by visual lines (caused bugs somewhere?)
    "nnoremap j gj
    "nnoremap k gk

    " Space for command line mode, currently have <Space> as <leader> instead
    "nnoremap <Space> :

    " keeps visual block selected after changing indention
    vmap > >gv
    vmap < <gv

    " Disables automatic commenting on newline
    autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

    " Automatically deletes all trailing whitespace on save
    " messes up markdown's double space for newline
    " seems better to let editorconfig handle this
    "autocmd BufWritePre * %s/\s\+$//e

    " Don't convert tabs to spaces for ~/qmk_firmware
    " using editorconfig for this...
    "au BufNewFile,BufRead ~/qmk_firmware/* setlocal noexpandtab

    " Pathogen
    execute pathogen#infect()

    " airline
    let g:airline_powerline_fonts = 1
    "let g:airline#extensions#tabline#enabled = 1
    " airline themes
    let g:airline_theme='base16color'
    "let g:airline_theme='ravenpower'
    "let g:airline_theme='base16_grayscale'
    "let g:airline_theme='base16_snazzy'
    "let g:airline_theme='base16_pop'
    "let g:airline_theme='base16_summerfruit'

    " indentline
    "let g:indentLine_color_term = 239
    "let g:indentLine_char = '┆'
    "let g:indentLine_char = '|'
    let g:indentLine_char = '⁝'
    "let g:indentLine_char = '⁞'
    "let g:indentLine_char = '⸽'

    " json-vim
    let g:vim_json_syntax_conceal = 0
    " indentLine is a little overzealous
    let g:indentLine_concealcursor=""

    " fix vertical split window borders
    set fillchars=vert:\|
    "highlight VertSplit ctermfg=2 ctermbg=0
    highlight VertSplit ctermfg=0 ctermbg=8
    "set vertsplit ctermfg=2
    "set vertsplit ctermbg=0

    " Line number color
    highlight LineNr ctermfg=8

    " Color column
    highlight ColorColumn ctermbg=8

    " Completion window color
    highlight Pmenu ctermbg=16 ctermfg=10
    highlight PmenuSel ctermbg=10 ctermfg=0

    " fzf
    let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --glob "!.git/*"'
    nmap // :BLines!<CR>

    " ##################
    " COC specific stuff
    " ##################

    " Some servers have issues with backup files, see #649.
    set nobackup
    set nowritebackup

    " Give more space for displaying messages.
    "set cmdheight=2

    " Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
    " delays and poor user experience.
    set updatetime=300

    " Don't pass messages to |ins-completion-menu|.
    set shortmess+=c

    "" Always show the signcolumn, otherwise it would shift the text each time
    "" diagnostics appear/become resolved.
    "if has("patch-8.1.1564")
    "  " Recently vim can merge signcolumn and number column into one
    "  set signcolumn=number
    "else
    "  set signcolumn=yes
    "endif

    " Use tab for trigger completion with characters ahead and navigate.
    " NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
    " other plugin before putting this into your config.
    inoremap <silent><expr> <TAB>
          \ pumvisible() ? "\<C-n>" :
          \ <SID>check_back_space() ? "\<TAB>" :
          \ coc#refresh()
    inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

    function! s:check_back_space() abort
      let col = col('.') - 1
      return !col || getline('.')[col - 1]  =~# '\s'
    endfunction

    " Use `[g` and `]g` to navigate diagnostics
    " Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
    nmap <silent> [g <Plug>(coc-diagnostic-prev)
    nmap <silent> ]g <Plug>(coc-diagnostic-next)

    " GoTo code navigation.
    nmap <silent> gd <Plug>(coc-definition)
    nmap <silent> gy <Plug>(coc-type-definition)
    nmap <silent> gi <Plug>(coc-implementation)
    nmap <silent> gr <Plug>(coc-references)

    " Use K to show documentation in preview window.
    nnoremap <silent> K :call <SID>show_documentation()<CR>

    function! s:show_documentation()
      if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
      elseif (coc#rpc#ready())
        call CocActionAsync('doHover')
      else
        execute '!' . &keywordprg . " " . expand('<cword>')
      endif
    endfunction

    " Highlight the symbol and its references when holding the cursor.
    autocmd CursorHold * silent call CocActionAsync('highlight')

    " Symbol renaming.
    nmap <leader>rn <Plug>(coc-rename)

    " Formatting selected code.
    xmap <leader>f  <Plug>(coc-format-selected)
    nmap <leader>f  <Plug>(coc-format-selected)

    augroup mygroup
      autocmd!
      " Setup formatexpr specified filetype(s).
      autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
      " Update signature help on jump placeholder.
      autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
    augroup end
