#!/usr/bin/env bash

function print_usage() {
    echo 'Usage: ./sync.sh [--pull|--push]'
    echo '  --pull, pull'
    echo '      pull dotfiles into git repo'
    echo '  --push, push'
    echo '      push dotfiles to $HOME'
}

REPO=${HOME}/dotfiles

[[ -z $1 || $# != 1 ]] && print_usage && exit 1

if [[ $1 == '--pull' || $1 == 'pull' ]]; then
    ### bash ###
    [[ -f ${HOME}/.bashrc ]] && cp -u ${HOME}/.bashrc $REPO
    [[ -f ${HOME}/.bash_aliases ]] && cp -u ${HOME}/.bash_aliases $REPO
    [[ -f ${HOME}/.profile ]] && cp -u ${HOME}/.profile $REPO

    # alternate aliases
    [[ -f ${HOME}/.aliasrc && ! -L ${HOME}/.aliasrc ]] && {
        cp -u ${HOME}/.aliasrc $REPO
    }

    ### zsh ###
    [[ -f ${HOME}/.zshrc ]] && cp -u ${HOME}/.zshrc $REPO
    [[ -f ${HOME}/.zshenv ]] && cp -u ${HOME}/.zshenv $REPO

    ### vim ###
    [[ -f ${HOME}/.vimrc ]] && cp -u ${HOME}/.vimrc $REPO
    [[ -d ${HOME}/.vim ]] && cp -ru ${HOME}/.vim $REPO

    ### neovim ###
    [[ ! -L ${HOME}/.config/nvim && -d ${HOME}/.config/nvim ]] && {
        cp -ru ${HOME}/.config/nvim $REPO
    }

    ### tmux ###
    [[ -f ${HOME}/.tmux.conf ]] && cp -u ${HOME}/.tmux.conf $REPO
    #[[ -f ${HOME}/.tmux.cb.conf ]] && cp -u ${HOME}/.tmux.cb.conf $REPO
    [[ -f ${HOME}/.tmuxtheme ]] && cp -u ${HOME}/.tmuxtheme $REPO

    ### i3 ###
    [[ -d ${HOME}/.config/i3 ]] && cp -ru ${HOME}/.config/i3 $REPO
    [[ -f ${HOME}/.i3status.conf ]] && cp -u ${HOME}/.i3status.conf $REPO

    ### polybar ###
    [[ -d ${HOME}/.config/polybar ]] && cp -ru ${HOME}/.config/polybar $REPO

    ### alacritty ###
    [[ -d ${HOME}/.config/alacritty ]] && cp -ru ${HOME}/.config/alacritty $REPO

    ### sync dotfiles script ###
    [[ -f ${HOME}/bin/sync_dotfiles.sh ]] && {
        cp -u ${HOME}/bin/sync_dotfiles.sh ${REPO}/sync.sh
    }
fi

if [[ $1 == '--push' || $1 == 'push' ]]; then
    ### bash ###
    [[ -f ${REPO}/.bashrc ]] && cp -u ${REPO}/.bashrc $HOME
    [[ -f ${REPO}/.bash_aliases ]] && cp -u ${REPO}/.bash_aliases $HOME
    [[ -f ${REPO}/.profile ]] && cp -u ${REPO}/.profile $HOME

    ### alternate aliases ###
    # dont overwrite any existing symlinks
    if [[ -f ${HOME}/.aliasrc && ! -L ${HOME}/.aliasrc && -f ${REPO}/.aliasrc ]]; then
        cp -u ${REPO}/.aliasrc ${HOME}/.aliasrc
    elif [[ ! -f ${HOME}/.aliasrc ]]; then
        ln -s ${HOME}/.bash_aliases ${HOME}/.aliasrc
    fi

    ### zsh ###
    [[ -f ${REPO}/.zshrc ]] && cp -u ${REPO}/.zshrc $HOME
    [[ -f ${REPO}/.zshenv ]] && cp -u ${REPO}/.zshenv $HOME

    ### vim ###
    [[ -f ${REPO}/.vimrc ]] && cp -u ${REPO}/.vimrc $HOME
    [[ -d ${REPO}/.vim ]] && cp -ru ${REPO}/.vim $HOME
    [[ ! -d ${HOME}/.vim-backup ]] && mkdir ${HOME}/.vim-backup

    ### neovim ###
    # create $HOME/.config/nvim as symlink to $HOME/.vim by default
    # dont overwrite any existing symlinks
    if [[ ! -e ${HOME}/.config/nvim && -d ${HOME}/.vim ]]; then
        ln -s ${HOME}/.vim ${HOME}/.config/nvim
    elif [[ ! -e ${HOME}/.vim/init.vim && -d ${HOME}/.vim ]]; then
        ln -s ${HOME}/.vimrc ${HOME}/.vim/init.vim
    fi

    ### tmux ###
    [[ -f ${REPO}/.tmux.conf ]] && cp -u ${REPO}/.tmux.conf $HOME
    #[[ -f ${REPO}/.tmux.cb.conf ]] && cp -u ${REPO}/.tmux.cb.conf $HOME
    [[ -f ${REPO}/.tmuxtheme ]] && cp -u ${REPO}/.tmuxtheme $HOME

    ### i3 ###
    [[ -d ${REPO}/.config/i3 ]] && cp -ru ${REPO}/.config/i3 $HOME
    [[ -f ${REPO}/.i3status.conf ]] && cp -u ${REPO}/.i3status.conf $HOME

    ### polybar ###
    [[ -d ${REPO}/.config/polybar ]] && cp -ru ${REPO}/.config/polybar $HOME

    ### alacritty ###
    [[ -d ${REPO}/.config/alacritty ]] && cp -ru ${REPO}/.config/alacritty $HOME

    ### sync dotfiles script ###
    if [[ ! -d ${HOME}/scripts ]]; then
        mkdir ${HOME}/scripts
    elif [[ -f ${REPO}/sync.sh ]]; then
        cp -u ${REPO}/sync.sh ${HOME}/scripts/sync_dotfiles.sh
    elif [[ -f ${HOME}/scripts/sync_dotfiles.sh && ! -x ${HOME}/scripts/sync_dotfiles.sh ]]; then
        chmod u+x ${HOME}/bin/sync_dotfiles.sh
    fi
fi

exit 0
